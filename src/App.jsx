import React, { createRef, Component } from 'react'
import './App.css'
import Card from './components/Card'
const accessKey = 'z0jwxLcfLdZs7KWUSaWN8WWi8PaguHh4ZVEjl_YiSsE'
let id = 0
export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      searchValue: '',
      cards: []
    }
  }
  fetchUrl = (url, type) => {
    fetch(url).then((data) => {
      return data.json()
    }).then((data) => {
      if (type == true) {
        data = data.results
      }
      this.setState({
        cards: data
      })
    })
  }
  search = () => {
    if (this.state.searchValue != '' && this.state.searchValue != undefined && this.state.searchValue != null) {
      let url = `https://api.unsplash.com/search/photos?page=1&query=${this.state.searchValue}&client_id=${accessKey}&per_page=10`
      this.fetchUrl(url, true)
    }
  }
  changeSearchValue = (event) => {
    this.setState({
      searchValue: event.target.value
    })
  }
  random = () => {
    let url = `https://api.unsplash.com/photos/random?client_id=${accessKey}&count=10`
    this.fetchUrl(url)
  }
  componentDidMount() {
    this.random()
  }
  componentDidUpdate(previousProps, previousState) {
    if (this.state.searchValue != '' && previousState.searchValue !== this.state.searchValue) {
      console.log("updated");
      this.search()
    }
  }

  render() {
    return (
      <>
        <header>
          <input id="search" type="text" value={this.state.searchValue} onChange={this.changeSearchValue} placeholder="Search" />
          <svg className='search-button' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            <path
              d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352c79.5 0 144-64.5 144-144s-64.5-144-144-144S64 128.5 64 208s64.5 144 144 144z" />
          </svg>
        </header>
        <main >
          <p className={this.state.cards.length > 0 ? "loading none" : "loading"}>LOADING...</p>
          {this.state.cards.map((card) => {
            return <Card key={id++} data={card} />
          })}
        </main>
      </>

    )
  }
}


