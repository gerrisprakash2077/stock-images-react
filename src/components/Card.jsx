import React, { Component } from 'react'
import './Card.css'
export default class Card extends Component {
    render() {
        return (
            <div className='card' style={{ backgroundImage: `url('${this.props.data.urls.regular}')` }}>
                <div className='.pop'></div>
            </div>
        )
    }
}
